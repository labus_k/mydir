#include <iostream>
#include <Windows.h>
#include <string>
#include <iomanip>

using namespace std;

namespace My 
{
	void GetError()
	{
		DWORD errCode = GetLastError();
		exit(errCode);
	}

	string CurrentDirectory()
	{
		char bufer[MAX_PATH];
		GetCurrentDirectory(sizeof(bufer), bufer);
		return (string(bufer) + "\\*");
	}
}

void GetHelp()
{
	cout << "����� ������ ������ � ������������ � �������� ��������." << endl << endl <<
		" mydir [����:][��� ��������]" << endl << endl << 
		"\t/? /h - ������� ������ ������� ��� ��������� ���� �������" << endl;
	exit(0);
}

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "RUS");
	char PipeName[] = "\\\\.\\pipe\\dirpipe";

	string filebuf;
	if (argc == 1)
	{
		filebuf = My::CurrentDirectory();
	}
	else
	{
		filebuf = argv[1];
		for (size_t i = 2; i < argc; i++)
		{
			filebuf += ' ';
			filebuf += argv[i];
		}
		if (filebuf.size() == 0)
		{
			filebuf = My::CurrentDirectory();
		}
		else if (filebuf.find("/?") != string::npos || filebuf.find("/h") != string::npos)
		{
			GetHelp();
		}
		else if (filebuf[filebuf.size() - 1] == '\"')
		{
			filebuf.replace(filebuf.end() - 1, filebuf.end(), "\\*");
		}
		else if (filebuf[filebuf.size() - 1] == '\\')
		{
			filebuf += '*';
		}		
		else if (filebuf[filebuf.size() - 1] != '*')
		{
			filebuf += "\\*";
		}
	}
	HANDLE hPipe = CreateFile(PipeName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
	
	if (hPipe == INVALID_HANDLE_VALUE)
	{
		cerr << endl << "������ �� �������!" << endl;
		CloseHandle(hPipe);
		My::GetError();
	}
	
	DWORD bTrans;
	size_t dataSize = filebuf.size() + 1;

	//�������� ����� �������� ���������

	BOOL Success = WriteFile(hPipe, &dataSize, sizeof(dataSize), &bTrans, NULL);
	if (Success == FALSE)
	{	
		cerr << endl << "��� ����� � ��������..." << endl;
		My::GetError();
	}

	//�������� ��������

	Success = WriteFile(hPipe, filebuf.c_str(), dataSize, &bTrans, NULL);
	if (Success == FALSE)
	{
		cerr << endl << "��� ����� � ��������..." << endl;
		My::GetError();
	}


	//�������� ����� �������� � ������

	Success = ReadFile(hPipe, &dataSize, sizeof(dataSize), &bTrans, NULL);
	if (Success == FALSE)
	{
		cerr << endl << "��� ����� � ��������..." << endl;
		My::GetError();
	}

	//�������� �����

	char* Data = new char[dataSize];
	Success = ReadFile(hPipe, Data, dataSize, &bTrans, NULL);
	if (Success == FALSE)
	{
		cerr << endl << "��� ����� � ��������..." << endl;
		delete[] Data;
		My::GetError();
	}
	cout << Data << endl;
	delete[] Data;
	CloseHandle(hPipe);
	return 0;
}


