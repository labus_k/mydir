#include <iostream>
#include <Windows.h>
#include <string>
#include <iomanip>
#include <sstream>


using namespace std;

namespace My
{
	void GetError()
	{
		DWORD errCode = GetLastError();
		char bufer[100];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errCode, 0, bufer, 100, NULL);
		cerr << "������ " << errCode << ": " << bufer << endl;
	}
	void GetError(DWORD errCode)
	{
		//DWORD errCode = GetLastError();
		char bufer[100];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errCode, 0, bufer, 100, NULL);
		cerr << "������ " << errCode << ": " << bufer << endl;
	}


	void StdoutFile(WIN32_FIND_DATA &fData, ostringstream& streamBuf)
	{
		SYSTEMTIME acTime;
		FileTimeToSystemTime(&fData.ftLastAccessTime, &acTime);
		uint64_t fSize = (fData.nFileSizeHigh * ((uint64_t)MAXDWORD + 1)
			+ fData.nFileSizeLow) >> 10;
		if (acTime.wDay < 10)
			streamBuf << '0';
		streamBuf << acTime.wDay << '.';
		if (acTime.wMonth < 10)
			streamBuf << '0';
		streamBuf << acTime.wMonth << '.' << acTime.wYear << "  ";
		if ((fData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
			streamBuf << "�������" << setw(10) << " ";
		else
			streamBuf << setw(12) << fSize << " ��  ";
		streamBuf << fData.cFileName << endl;
	}
}


struct threadParam
{
	HANDLE thPipe;
	HANDLE *thEvents;
	OVERLAPPED *toOverlap;
	threadParam() : thPipe(NULL), thEvents(NULL), toOverlap(NULL) {	}
	~threadParam()
	{
		if (thEvents != NULL)
		{
			delete[] thEvents;
		}
	}
};

DWORD WINAPI ClientWork(LPVOID);




char PipeName[] = "\\\\.\\pipe\\dirpipe";

int main()
{
	setlocale(LC_ALL, "RUS");
	HANDLE hPipe = NULL, hOverEvent = NULL, hQuitEvent = NULL;
	OVERLAPPED oOverlap = {};
	hOverEvent = CreateEvent(
		NULL,
		TRUE,
		TRUE,
		NULL
	);
	hQuitEvent = CreateEvent(
		NULL,
		FALSE,
		FALSE,
		NULL
	);

	if (hOverEvent == NULL || hQuitEvent == NULL)
	{
		My::GetError();
		return 4;
	}

	oOverlap.hEvent = hOverEvent;
			
	hPipe = CreateNamedPipe(
		"\\\\.\\pipe\\dirpipe", 
		PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, 
		1, 
		0, 
		0, 
		5000, 
		NULL
	);

	if (hPipe == INVALID_HANDLE_VALUE)
	{
		My::GetError();
		return 3;
	}

	threadParam tParam;
	tParam.thEvents = new HANDLE[2];
	tParam.thEvents[0] = hOverEvent;
	tParam.thEvents[1] = hQuitEvent;
	tParam.thPipe = hPipe;
	tParam.toOverlap = &oOverlap;
	DWORD threadID;
	HANDLE hThread = CreateThread(
		NULL,
		0,
		ClientWork,
		&tParam,
		0,
		&threadID
	);
	if (hThread == NULL)
	{
		My::GetError();
		exit(3);
	}
	char COMMAND = 'x';
	do
	{
		COMMAND = getchar();
	} while (COMMAND != 'q');
	SetEvent(hQuitEvent);
	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hPipe);
	CloseHandle(hThread);
	return 0;
}

DWORD WINAPI ClientWork(LPVOID lpParametr)
{
	size_t dataSize;
	DWORD dwWait, dwError;
	BOOL Success, fNext;
	WIN32_FIND_DATA FindFileData;
	string filename;
	HANDLE hFind;
	threadParam *tParam = reinterpret_cast<threadParam*>(lpParametr);
	while (1)
	{
		cout << "�������� �������..." << endl;
		Success = ConnectNamedPipe(tParam->thPipe, tParam->toOverlap);

		if (Success == FALSE)
		{
			dwError = GetLastError();
			if (dwError != ERROR_IO_PENDING && dwError != ERROR_PIPE_CONNECTED)
			{
				My::GetError(dwError);
				continue;
			}
		}
		dwWait = WaitForMultipleObjects(2,
			tParam->thEvents,
			FALSE,
			INFINITE
		);
		dwWait -= WAIT_OBJECT_0;
		switch (dwWait)
		{
			case 0:
				break;
			case 1:
				return 5;
			default:
			{
				My::GetError();
				continue;
			}
		}
		cout << "���������� �����������! ��������� �������..." << endl;

		//�������� ����� �������� � ���������

		Success = ReadFile(
			tParam->thPipe,
			&dataSize,
			sizeof(dataSize),
			NULL,
			tParam->toOverlap
		);
		if (Success == FALSE)
		{
			dwError = GetLastError();
			if (dwError != ERROR_IO_PENDING)
			{
				My::GetError(dwError);
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}

		dwWait = WaitForMultipleObjects(
			2,
			tParam->thEvents,
			FALSE,
			INFINITE
		);

		switch (dwWait)
		{
			case 0:
				break;
			case 1:
			{
				DisconnectNamedPipe(tParam->thPipe);
				return 1;
			}
			default:
			{
				My::GetError();
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}

		char *command = new char[dataSize];

		//�������� �������

		Success = ReadFile(
			tParam->thPipe,
			command,
			dataSize,
			NULL,
			tParam->toOverlap
		);

		if (Success == FALSE)
		{
			dwError = GetLastError();
			if (dwError != ERROR_IO_PENDING)
			{
				My::GetError(dwError);
				DisconnectNamedPipe(tParam->thPipe);
				delete[] command;
				continue;
			}
		}

		dwWait = WaitForMultipleObjects(
			2,
			tParam->thEvents,
			FALSE,
			INFINITE
		);

		switch (dwWait)
		{
			case 0:
				break;
			case 1:
			{
				DisconnectNamedPipe(tParam->thPipe);
				return 1;
			}
			default:
			{
				My::GetError();
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}


		cout << "������� �������: \"" << command << '\"' << endl << "������� ������������ ������" << endl;
		ostringstream outbufer;
		filename = command;
		delete[] command;
		hFind = FindFirstFile(filename.c_str(), &FindFileData);

		if (hFind == INVALID_HANDLE_VALUE)
		{
			My::GetError();
			outbufer << "�������� ��� �����, ����� ��� �������!\n";
		}

		else

		{
			const DWORD pathNameSize = 4096;
			char pathBuf[pathNameSize];
			if (GetFullPathName(filename.c_str(), pathNameSize, pathBuf, NULL) == 0)
			{
				My::GetError();
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}

			string pathName(pathBuf);
			pathName.erase(pathName.find_last_of('\\'));
			outbufer << endl << "���������� ����� " << pathName << endl << endl;

			My::StdoutFile(FindFileData, outbufer);
			fNext = FindNextFile(hFind, &FindFileData);		//�������� ������ ����� � ������
			while (fNext != FALSE)
			{
				My::StdoutFile(FindFileData, outbufer);
				fNext = FindNextFile(hFind, &FindFileData);
			}
		}

		cout << "����� �����������! �������� ������ �������..." << endl;
		dataSize = outbufer.str().size() + 1;

		//���������� ����� �������� � ������

		Success = WriteFile(
			tParam->thPipe,
			&dataSize,
			sizeof(dataSize),
			NULL,
			tParam->toOverlap
		);

		if (Success == FALSE)
		{
			dwError = GetLastError();
			if (dwError != ERROR_IO_PENDING)
			{
				My::GetError(dwError);
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}

		dwWait = WaitForMultipleObjects(
			2,
			tParam->thEvents,
			FALSE,
			INFINITE
		);

		switch (dwWait)
		{
			case 0:
				break;
			case 1:
			{
				DisconnectNamedPipe(tParam->thPipe);
				return 1;
			}
			default:
			{
				My::GetError();
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}

		//���������� �����

		Success = WriteFile(
			tParam->thPipe,
			outbufer.str().c_str(),
			dataSize,
			NULL,
			tParam->toOverlap
		);

		if (Success == FALSE)
		{
			dwError = GetLastError();
			if (dwError != ERROR_IO_PENDING)
			{
				My::GetError(dwError);
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}

		dwWait = WaitForMultipleObjects(
			2,
			tParam->thEvents,
			FALSE,
			INFINITE
		);
		
		switch (dwWait)
		{
			case 0:
				break;
			case 1:
			{
				DisconnectNamedPipe(tParam->thPipe);
				return 1;
			}
			default:
			{
				My::GetError();
				DisconnectNamedPipe(tParam->thPipe);
				continue;
			}
		}
		cout << "�������� ����������!" << endl << endl;
		DisconnectNamedPipe(tParam->thPipe);
	}
}
